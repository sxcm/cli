kind: Template
apiVersion: template.openshift.io/v1
metadata:
  name: sxcm-demo-vpa-project
  annotations:
    openshift.io/display-name: Demo STARTX - VerticalPodAutoscaler
    openshift.io/documentation-url: https://sxcm.readthedocs.io/en/latest/demo/vpa
    openshift.io/support-url: https://github.com/startxfr/sxcm/issues/new
    openshift.io/generated-by: sxcm
    openshift.io/provider-display-name: STARTX
    description:
      Deployment of an VerticalPodAutoscaler and a sample application using this infrastructure to 
      demonstrate the VerticalPodAutoscaler capacities running under an openshift 4 environment
    iconClass: icon-load-balancer
    tags: startx,vpa,demo,dev
  labels:
    template: sxcm-demo-vpa-project
message: |-
  Your demo project ${NS} for VerticalPodAutoscaler technology is now created.

  Scope          : ${SCOPE}
  Environment    : ${ENV}
  Service        : vpa
  Project        : ${NS}

labels:
  template: sxcm-demo-vpa-project
  app.kubernetes.io/managed-by: sxcm
objects:
- kind: Application
  apiVersion: argoproj.io/v1alpha1
  metadata:
    name: ${NS}-instance
    namespace: "${ARGOCD_NS}"
    labels:  &basic_labels
      app.startx.fr/scope: "${SCOPE}"
      app.startx.fr/cluster: "${CLUSTER}"
      app.startx.fr/component: "${NS}"
      app.kubernetes.io/part-of: ${NS}
      app.kubernetes.io/version: "${VERSION}"
      app.kubernetes.io/component: "${NS}-project"
      app.kubernetes.io/name: "${NS}-instance-application"
    annotations: &basic_annotations
      openshift.io/generated-by: sxcm
    finalizers:
      - resources-finalizer.argocd.argoproj.io
  spec:
    destination:
      namespace: "${NS}"
      server: 'https://kubernetes.default.svc'
    info:
      - name: teammail
        value: dev@startx.fr
    project: cluster-admin
    source:
      path: charts/cluster-vpa/
      repoURL: 'https://github.com/startxfr/helm-repository.git'
      targetRevision: "${HELM_RELEASE}"
      helm:
        valueFiles:
          - values-demo.yaml
        parameters:
          - name: context.scope
            value: "${SCOPE}"
          - name: context.cluster
            value: "${CLUSTER}"
          - name: context.environment
            value: "${ENV}"
          - name: context.version
            value: "${VERSION}"
          - name: context.app
            value: "startx-startx"
          - name: project.enabled
            value: "false"
          - name: project.project.requester
            value: "sxcm"
          - name: operator.enabled
            value: "false"
          - name: vpa.enabled
            value: "true"
    syncPolicy:
      automated:
        prune: true
        selfHeal: true
      syncOptions:
        - ApplyOutOfSyncOnly=true
        - CreateNamespace=false
        - Validate=true
      retry:
        limit: 3
        backoff:
          duration: 60s
          factor: 1
          maxDuration: 60s
parameters:
  - name: NS
    displayName: The namespace where objects goes to
    description: "Namespace to place objects to"
    value: demo-vpa
    required: true
  - name: ARGOCD_NS
    displayName: The namespace where argocd server goes to
    description: "Namespace to place argocd server to"
    value: openshift-gitops
    required: true
  - name: SCOPE
    displayName: Project scope
    description: "Project scope (ex: sxv4)"
    value: demo
    required: true
  - name: CLUSTER
    displayName: Cluster name
    description: "Name of the current cluster  (ex: sxsf)"
    value: sxsf
    required: true
  - name: ENV
    displayName: Project environment
    description: "Project environment (ex: dev, factory, preprod or prod)"
    value: dev
    required: true
  - name: HELM_RELEASE
    displayName: Helm repository release
    description: "Branch or release name for the helm repository holding all charts"
    value: devel
  - name: VERSION
    displayName: Project version
    description: "Project deployed release"
    value: "1.0.0-dev"
    required: true
  - name: COMPONENT
    displayName: Project service
    description: "Project service"
    value: demo
    required: true
