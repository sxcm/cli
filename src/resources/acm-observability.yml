kind: Template
apiVersion: template.openshift.io/v1
metadata:
  name: sxcm-cluster-acm-observability-project
  annotations:
    openshift.io/display-name: STARTX Advanced Cluster Management (admin)
    description: Deploy cluster-wide resources to enable ACM support for multi-cluster management
    iconClass: icon-openshift
    tags: startx,cluster,config,admin,acm,multi-cloud,management
    openshift.io/provider-display-name: STARTX
    openshift.io/generated-by: sxcm
    sxcm_console_timeout: "30"
  labels:
    template: sxcm-cluster-acm-observability-project
    app.kubernetes.io/name: "sxcm-cluster-acm-observability-project"
    app.kubernetes.io/managed-by: sxcm
message: |-
  Your cluster-wide ACM support for multi-cluster management is now enabled.

  Scope             : ${SCOPE}
  Cluster           : ${CLUSTER}
  Operator          : advanced-cluster-management.2.1.2
labels:
  template: sxcm-cluster-acm-observability-project
  app.kubernetes.io/managed-by: sxcm
objects:
- kind: Application
  apiVersion: argoproj.io/v1alpha1
  metadata:
    name: startx-cluster-acm-observability-instance
    namespace: "${ARGOCD_NS}"
    labels:  &basic_labels
      app.startx.fr/scope: "${SCOPE}"
      app.startx.fr/cluster: "${CLUSTER}"
      app.startx.fr/component: "cluster-acm"
      app.kubernetes.io/name: "startx-cluster-acm-observability-instance"
      app.kubernetes.io/part-of: ${CLUSTER}
      app.kubernetes.io/version: "${VERSION}"
      app.kubernetes.io/component: "cluster-acm"
    annotations: &basic_annotations
      openshift.io/generated-by: sxcm
      # argocd.argoproj.io/manifest-generate-paths: .
    finalizers:
      - resources-finalizer.argocd.argoproj.io
  spec:
    destination:
      namespace: "${NS}"
      server: 'https://kubernetes.default.svc'
    info:
      - name: teammail
        value: dev@startx.fr
    project: cluster-admin
    source:
      path: charts/cluster-acm/
      repoURL: 'https://github.com/startxfr/helm-repository.git'
      targetRevision: "${HELM_RELEASE}"
      helm:
        valueFiles:
          - values-startx-observability.yaml
        parameters:
          - name: context.scope
            value: "${SCOPE}"
          - name: context.cluster
            value: "${CLUSTER}"
          - name: context.environment
            value: "${ENV}"
          - name: context.version
            value: "${VERSION}"
          - name: context.app
            value: "openshift-acm"
          - name: project.enabled
            value: "false"
          - name: projectObservability.enabled
            value: "false"
          - name: observability.enabled
            value: "true"
          - name: observability.alertmanager_config.enabled
            value: "true"
          - name: observability.custom_rules.enabled
            value: "true"
          - name: observability.custom_metrics.enabled
            value: "true"
          - name: observability.bucket.s3bucket
            value: "s3bucket_CHANGEME"
          - name: observability.bucket.s3host
            value: "s3.amazonaws.com"
          - name: observability.bucket.s3accesskey
            value: "s3accesskey_CHANGEME"
          - name: observability.bucket.s3secretkey
            value: "s3secretkey_CHANGEME"
    syncPolicy:
      automated:
        prune: true
        selfHeal: false
      syncOptions:
        - ApplyOutOfSyncOnly=true
        - CreateNamespace=false
        - Validate=true
      retry:
        limit: 3
        backoff:
          duration: 60s
          factor: 1
          maxDuration: 60s
parameters:
  - name: ARGOCD_NS
    displayName: The namespace where argocd server goes to
    description: "Namespace to place argocd server to"
    value: openshift-gitops
  - name: NS
    displayName: The namespace where objects goes to
    description: "Namespace to place objects to"
    value: open-cluster-management
  - name: SCOPE
    displayName: Project scope
    description: "Project scope (ex: sxv4)"
    value: startx
  - name: CLUSTER
    displayName: Cluster name
    description: "Name of the current cluster  (ex: sxsf)"
    value: sxsf
  - name: ENV
    displayName: Project environment
    description: "Project environment (ex: dev, factory, preprod or prod)"
    value: dev
  - name: VERSION
    displayName: Project version
    description: "Project deployed release"
    value: 4.13.x-dev
  - name: CLUSTER_PROFILE
    displayName: Name of the cluster profile
    description: "The name of the cluster profile"
    value: default
  - name: DOCKERHUB_LOGIN
    displayName: Login to use for pulling images from the dockerhub registry
    description: "The login to use for pulling images from the dockerhub registry"
    value: myDockerhubLogin
  - name: DOCKERHUB_PWD
    displayName: Password to use for pulling images from the dockerhub registry
    description: "The Password coresponding to the login for pulling images from the dockerhub registry"
    value: myDockerhubPassword
  - name: QUAYIO_LOGIN
    displayName: Login to use for pulling images from the quay.io registry
    description: "The login to use for pulling images from the quay.io registry"
    value: myQuayioLogin
  - name: QUAYIO_PWD
    displayName: Password to use for pulling images from the quay.io registry
    description: "The Password coresponding to the login for pulling images from the quay.ioi registry"
    value: myQuayioPassword
  - name: HELM_RELEASE
    displayName: Helm repository release
    description: "Branch or release name for the helm repository holding all charts"
    value: main
