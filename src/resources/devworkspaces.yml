kind: Template
apiVersion: template.openshift.io/v1
metadata:
  name: sxcm-cluster-devworkspaces-project
  annotations:
    openshift.io/display-name: STARTX cluster DevWorkspaces (admin)
    description: Deploy cluster-wide resources to enable DevWorkspaces according to sxcm definitions
    iconClass: icon-openshift
    tags: startx,cluster,config,admin,devworkspaces
    openshift.io/provider-display-name: STARTX
    openshift.io/generated-by: sxcm
    sxcm_console_timeout: "20"
  labels:
    template: sxcm-cluster-devworkspaces-project
    app.kubernetes.io/name: "sxcm-cluster-devworkspaces-project"
    app.kubernetes.io/managed-by: sxcm
message: |-
  Your code ready devworkspaces is now enabled

  Scope             : ${SCOPE}
  Cluster           : ${CLUSTER}
  Operator          : devworkspaces.v3.1.0
labels:
  template: sxcm-cluster-devworkspaces-project
  app.kubernetes.io/managed-by: sxcm
objects: 
- kind: AppProject
  apiVersion: argoproj.io/v1alpha1
  metadata:
    name: ${NS}
    namespace: "${ARGOCD_NS}"
    labels:  &basic_labels
      app.startx.fr/scope: "${SCOPE}"
      app.startx.fr/cluster: "${CLUSTER}"
      app.startx.fr/component: "cluster-devworkspaces"
      app.kubernetes.io/name: "${NS}-appproject"
      app.kubernetes.io/part-of: ${CLUSTER}
      app.kubernetes.io/version: "${VERSION}"
      app.kubernetes.io/component: "cluster-devworkspaces"
    annotations: &basic_annotations
      openshift.io/generated-by: sxcm
    finalizers:
      - resources-finalizer.argocd.argoproj.io
  spec:
    sourceRepos:
      - '*'
    destinations:
      - namespace: ${NS}
        server: '*'
    clusterResourceWhitelist:
      - group: '*'
        kind: '*'
    roles:
    - name: dev
      description: Read-only privileges to group dev in ${NS} project
      policies:
      - g, dev, role:readonly
      - p, role:startx-dev, *, get, .${NS}/*, allow
      groups:
      - dev
    - name: ops
      description: Read-only (and sync) privileges to group ops in ${NS} project
      policies:
      - p, role:startx-ops, clusters, get, .${NS}/*, allow
      - p, role:startx-ops, projects, get, .${NS}/*, allow
      - p, role:startx-ops, applications, get, .${NS}/*, allow
      - p, role:startx-ops, applications, sync, .${NS}/*, allow
      - p, role:startx-ops, repositories, get, .${NS}/*, allow
      - p, role:startx-ops, repositories, sync, .${NS}/*, allow
      - p, role:startx-ops, certificates, get, .${NS}/*, allow
      - g, ops, role:startx-ops
      groups:
      - ops
    - name: devops
      description: Read-only privileges to group devops in ${NS} project
      policies:
      - p, role:startx-devops, clusters, get, .${NS}/*, allow
      - p, role:startx-devops, projects, get, .${NS}/*, allow
      - p, role:startx-devops, applications, get, .${NS}/*, allow
      - p, role:startx-devops, repositories, get, .${NS}/*, allow
      - p, role:startx-devops, certificates, get, .${NS}/*, allow
      - g, devops, role:startx-devops
      groups:
      - devops
    - name: admin
      description: All privileges to group admin in ${NS} project
      policies:
      - p, role:startx-admin, *, *, ${NS}/*, allow
      - g, system:cluster-admins, role:admin
      groups:
      - my-oidc-group
      - system:cluster-admins
      - admin
- kind: Application
  apiVersion: argoproj.io/v1alpha1
  metadata:
    name: startx-cluster-workspace-project
    namespace: "${ARGOCD_NS}"
    labels:
      <<: *basic_labels
      app.kubernetes.io/name: "startx-cluster-workspace-project-application"
    annotations:
      <<: *basic_annotations
      # argocd.argoproj.io/manifest-generate-paths: .
    finalizers:
      - resources-finalizer.argocd.argoproj.io
  spec:
    destination:
      namespace: "${NS}"
      server: 'https://kubernetes.default.svc'
    info:
      - name: teammail
        value: dev@startx.fr
    project: cluster-admin
    source:
      path: charts/cluster-devworkspaces/
      repoURL: 'https://github.com/startxfr/helm-repository.git'
      targetRevision: devel
      helm:
        releaseName: startx-workspace-project
        valueFiles:
          - values-startx.yaml
        parameters:
          - name: context.scope
            value: "${SCOPE}"
          - name: context.cluster
            value: "${CLUSTER}"
          - name: context.environment
            value: "${ENV}"
          - name: context.version
            value: "${VERSION}"
          - name: context.app
            value: "${NS}"
          - name: project.project.name
            value: "${NS}"
          - name: project.project.requester
            value: "sxcm"
          - name: project.enabled
            value: "true"
    syncPolicy:
      automated:
        prune: true
        selfHeal: false
      syncOptions:
        - ApplyOutOfSyncOnly=true
        - CreateNamespace=false
        - Validate=true
      retry:
        limit: 5
        backoff:
          duration: 5s
          factor: 2
          maxDuration: 30s
- kind: Application
  apiVersion: argoproj.io/v1alpha1
  metadata:
    name: startx-cluster-workspace-operator
    namespace: "${ARGOCD_NS}"
    labels:
      <<: *basic_labels
      app.kubernetes.io/name: "startx-cluster-workspace-operator-application"
    annotations:
      <<: *basic_annotations
      # argocd.argoproj.io/manifest-generate-paths: .
    finalizers:
      - resources-finalizer.argocd.argoproj.io
  spec:
    destination:
      namespace: "${NS}"
      server: 'https://kubernetes.default.svc'
    info:
      - name: teammail
        value: dev@startx.fr
    project: cluster-admin
    source:
      path: charts/cluster-devworkspaces/
      repoURL: 'https://github.com/startxfr/helm-repository.git'
      targetRevision: devel
      helm:
        releaseName: startx-workspace-operator
        valueFiles:
          - values-startx.yaml
        parameters:
          - name: context.scope
            value: "${SCOPE}"
          - name: context.cluster
            value: "${CLUSTER}"
          - name: context.environment
            value: "${ENV}"
          - name: context.version
            value: "${VERSION}"
          - name: context.app
            value: "${NS}"
          - name: project.project.name
            value: "${NS}"
          - name: operator.enabled
            value: "true"
    syncPolicy:
      automated: 
        prune: false
        selfHeal: false
      syncOptions:
        - ApplyOutOfSyncOnly=true
        - CreateNamespace=false
        - Validate=true
      retry:
        limit: 5
        backoff:
          duration: 5s
          factor: 2
          maxDuration: 20s
    ignoreDifferences:
      - group: operators.coreos.com
        kind: OperatorGroup
        jsonPointers: [ "/metadata/annotations/olm.providedAPIs" ]
- kind: Application
  apiVersion: argoproj.io/v1alpha1
  metadata:
    name: startx-cluster-workspace-instance
    namespace: "${ARGOCD_NS}"
    labels:
      <<: *basic_labels
      app.kubernetes.io/name: "startx-cluster-workspace-instance-application"
    annotations:
      <<: *basic_annotations
      # argocd.argoproj.io/manifest-generate-paths: .
    finalizers:
      - resources-finalizer.argocd.argoproj.io
  spec:
    destination:
      namespace: "${NS}"
      server: 'https://kubernetes.default.svc'
    info:
      - name: teammail
        value: dev@startx.fr
    project: cluster-admin
    source:
      path: charts/cluster-devworkspaces/
      repoURL: 'https://github.com/startxfr/helm-repository.git'
      targetRevision: devel
      helm:
        releaseName: startx-workspace-instance
        valueFiles:
          - values-startx.yaml
        parameters:
          - name: context.scope
            value: "${SCOPE}"
          - name: context.cluster
            value: "${CLUSTER}"
          - name: context.environment
            value: "${ENV}"
          - name: context.version
            value: "${VERSION}"
          - name: context.app
            value: "${NS}"
          - name: project.project.name
            value: "${NS}"
          - name: devworkspaces.enabled
            value: "true"
    syncPolicy:
      automated: 
        prune: false
        selfHeal: false
      syncOptions:
        - ApplyOutOfSyncOnly=true
        - CreateNamespace=false
        - Validate=true
      retry:
        limit: 5
        backoff:
          duration: 5s
          factor: 2
          maxDuration: 20s
parameters:
  - name: ARGOCD_NS
    displayName: The namespace where argocd server goes to
    description: "Namespace to place argocd server to"
    value: openshift-gitops
  - name: NS
    displayName: The namespace where objects goes to
    description: "Namespace to place objects to"
    value: openshift-devworkspaces
  - name: SCOPE
    displayName: Project scope
    description: "Project scope (ex: sxv4)"
    value: startx
  - name: CLUSTER
    displayName: Cluster name
    description: "Name of the current cluster  (ex: sxsf)"
    value: sxsf
  - name: ENV
    displayName: Project environment
    description: "Project environment (ex: dev, factory, preprod or prod)"
    value: dev
  - name: VERSION
    displayName: Project version
    description: "Project deployed release"
    value: 4.13.x-dev
  - name: CLUSTER_PROFILE
    displayName: Name of the cluster profile
    description: "The name of the cluster profile"
    value: default
  - name: DOCKERHUB_LOGIN
    displayName: Login to use for pulling images from the dockerhub registry
    description: "The login to use for pulling images from the dockerhub registry"
    value: myDockerhubLogin
  - name: DOCKERHUB_PWD
    displayName: Password to use for pulling images from the dockerhub registry
    description: "The Password coresponding to the login for pulling images from the dockerhub registry"
    value: myDockerhubPassword
  - name: QUAYIO_LOGIN
    displayName: Login to use for pulling images from the quay.io registry
    description: "The login to use for pulling images from the quay.io registry"
    value: myQuayioLogin
  - name: QUAYIO_PWD
    displayName: Password to use for pulling images from the quay.io registry
    description: "The Password coresponding to the login for pulling images from the quay.ioi registry"
    value: myQuayioPassword
  - name: HELM_RELEASE
    displayName: Helm repository release
    description: "Branch or release name for the helm repository holding all charts"
    value: devel
